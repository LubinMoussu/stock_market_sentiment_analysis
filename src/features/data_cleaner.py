import re

import numpy as np
from pandas import DataFrame


class DataCleaner:
    categorical_missing_values_handler = "drop_row"
    missing_values = [np.nan]

    def __init__(self, df):
        if not isinstance(df, DataFrame):
            raise TypeError
        self.df = df

    def shuffle_df(self):
        self.df = self.df.sample(frac=1).reset_index(drop=True)

    def clean_col_names(self):
        """
        Simple function to convert the column
        names of a dataframe to snake_case and lower case.
        """
        # Get all the col names as lower and snake_case in a list
        new_col_names = [
            column.strip().replace(' ', '_').lower().rstrip("\n") for column in self.df.columns
        ]
        # Rename the column names
        self.df.columns = new_col_names

    def fill_categorical_missing_values(self):
        """

        :return:
        """
        if self.categorical_missing_values_handler == "drop_row":
            self.df.dropna(axis=0, inplace=True)
        elif self.categorical_missing_values_handler == "drop_column":
            self.df.dropna(axis=1, inplace=True)
        elif self.categorical_missing_values_handler == "replace_mfv":
            self.df = self.df.fillna(self.df.mode().iloc[0], inplace=True)

    def delete_rows_with_outliers(self):
        """
        Remove rows given matching conditions
        :return:
        """
        pass

    def object_dtype_column_list(self):
        return [column for column in self.df.columns.tolist() if self.df[column].dtype == "object"]

    def normalize_textual_data(self, string: str):
        """

        :param string:
        :return:
        """
        if string not in self.missing_values:
            clean_string = re.sub("b[(')]", '', string)
            clean_string = re.sub('b[(")]', '', clean_string)
            clean_string = re.sub("\'", "", clean_string)

            return str(clean_string).lower().strip()
        return string

    def clean_categorical_column(self):
        """
        Map
        :return:
        """
        object_column = self.object_dtype_column_list()  # get categorical columns
        self.df.loc[:, object_column] = self.df.loc[:, object_column].applymap(
            self.normalize_textual_data)  # apply normalize_textual_data to all categorical values

    def drop_full_duplicates(self):
        self.df.drop_duplicates(inplace=True)

    def clean_df(self):

        self.clean_col_names()
        self.fill_categorical_missing_values()
        self.delete_rows_with_outliers()
        self.clean_categorical_column()  # Apply normalization to text values
        self.drop_full_duplicates()
        self.shuffle_df()
        return self.df
