from pandas import DataFrame

from textblob import TextBlob
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


class DataEnricher:
    def __init__(self, df):
        if not isinstance(df, DataFrame):
            raise TypeError
        self.df = df

    @staticmethod
    def get_subjectivity(text):
        return TextBlob(text).sentiment.subjectivity

    @staticmethod
    def get_polarity(text):
        return TextBlob(text).sentiment.polarity

    @staticmethod
    def get_sentiment_intensity_analysis(text):
        sia = SentimentIntensityAnalyzer()
        return sia.polarity_scores(text)

    def get_sentiment_scores_per_day(self):
        compound = []
        neg = []
        pos = []
        neu = []
        for _, row in self.df.iterrows():
            sia = self.get_sentiment_intensity_analysis(text=row["combined_top"])
            compound.append(sia["compound"])
            neg.append(sia["neg"])
            pos.append(sia["pos"])
            neu.append(sia["neu"])
        return compound, neg, pos, neu

    def enrich_data(self):
        # Combine all articles
        column_to_connect = [f"top{i}" for i in range(1, 26)]
        headlines = [
            "; ".join(str(row[x]) for x in column_to_connect)
            for _, row in self.df.iterrows()
        ]
        self.df["combined_top"] = headlines

        # get subjectivity
        self.df["subjectivity"] = self.df["combined_top"].apply(self.get_subjectivity)
        # get polarity
        self.df["polarity"] = self.df["combined_top"].apply(self.get_polarity)
        self.df["compound"], self.df["negative"], self.df["positive"], self.df[
            "neutral"] = self.get_sentiment_scores_per_day()
        return self.df
