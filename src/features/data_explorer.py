from pandas import DataFrame


class DataExplorer:
    def __init__(self, df):
        if not isinstance(df, DataFrame):
            raise TypeError
        self.df = df

    def get_column_list_with_unique_value(self):
        """
        Get column list of column containing only one variant
        :return:
        """
        return [column for column in self.df.columns.tolist() if len(self.df[column].unique()) <= 1]

    def explore_data(self):
        print(f"Shape: {self.df.shape}")
        print(f"Columns: {', '.join(list(self.df.columns))}")
        # column_type = [self.df[column].dtype for column in self.df.columns.tolist()]
        # print(column_type)

        column_with_unique_value = self.get_column_list_with_unique_value()
        print(f"Column(s) with 1 unique values: {', '.join(column_with_unique_value)}")

        column_with_almost_unique_values = [column for column in self.df.columns.tolist() if
                                            len(self.df[column].unique()) < 3]
        print(f"Column(s) with 2 or less unique values: {', '.join(column_with_almost_unique_values)}")

        return self.df
