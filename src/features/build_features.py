import os
from collections import Counter

import numpy as np
import pandas as pd

import src

paths = src.config.DEFAULT.PATHS
DELIMITER = ","
# import nltk for stopwords
from nltk.corpus import stopwords

stop_words = set(stopwords.words('english'))

from src.features.data_enricher import DataEnricher
from src.features.data_cleaner import DataCleaner
from src.features.data_explorer import DataExplorer


class BuildFeatures:
    fileout = "cleaned_df.csv"
    missing_values = [np.nan]

    def __init__(self, filepath):
        self.df = self.load_df_from(filepath=filepath)

    @staticmethod
    def load_df_from(filepath):
        return pd.read_csv(filepath, delimiter=DELIMITER, index_col=False)

    def is_any_missing_values_across_columns(self):
        return self.df.isnull()

    def is_any_missing_values(self):
        return self.df.isnull().values.any()

    def get_count_of_missing_values(self):
        return self.df.isnull().sum().sum()

    def get_count_of_rows_with_missing_values(self):
        return self.df.shape[0] - self.df.dropna().shape[0]

    def get_count_of_columns_with_missing_values(self):
        return self.df.shape[1] - self.df.dropna(axis='columns').shape[1]

    def get_ratio_of_rows_with_missing_values(self):
        return round(self.get_count_of_rows_with_missing_values() / self.df.shape[0] * 100, 3)

    def get_ratio_of_columns_with_missing_values(self):
        return round(self.get_count_of_columns_with_missing_values() / self.df.shape[1] * 100, 3)

    def fill_missing_values_with_column_mean(self):
        self.df = self.df.fillna(self.df.mean())

    def get_column_name_with_missing_values(self):
        column_missing_values = self.df.isnull().sum()
        return column_missing_values.loc[column_missing_values != 0].index.values

    def get_column_occurrences(self, column_name: str):
        column_values = self.df[column_name]
        return Counter(column_values)

    def get_df_column_occurrences(self):
        column_list = self.df.columns.tolist()
        return {elt: self.get_column_occurrences(column_name=elt) for elt in column_list}

    def test(self):
        # print(self.df.combined_top)
        pass

    def workflow(self):
        data_cleaner = DataCleaner(df=self.df)
        cleaned_df = data_cleaner.clean_df()

        data_enricher = DataEnricher(df=cleaned_df)
        enriched_df = data_enricher.enrich_data()

        self.df = enriched_df
        self.test()
        data_explorer = DataExplorer(df=self.df)
        self.df = data_explorer.explore_data()

        # self.df.to_csv(self.fileout, sep=DELIMITER, encoding='utf-8', index=False)
        return self.df


if __name__ == "__main__":
    reddit_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "RedditNews.csv")
    combined_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "Combined_News_DJIA.csv")
    upload_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "upload_DJIA_table.csv")

    # Combine df of combined_news_file and upload_news_file
    build_features = BuildFeatures(filepath=combined_news_file)
    df2 = build_features.load_df_from(upload_news_file)
    build_features.df = build_features.df.merge(df2, how="inner", on="Date")

    build_features.workflow()
