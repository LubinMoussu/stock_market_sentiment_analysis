class FeaturesImportance:
    def __init__(self):
        self.model = None
        self.feature_list = []

    def set_model(self, model):
        self.model = model

    def set_feature_list(self, feature_list):
        self.feature_list = feature_list

    def get_feature_importance(self):
        if hasattr(self.model, 'feature_importances_'):
            importances = self.model.feature_importances_
            feature_importances = list(zip(self.feature_list, importances))
            feature_importances = sorted(feature_importances, key=lambda x: x[1],
                                         reverse=True)  # Print out the feature and importances
            for pair in feature_importances:
                print('Variable: {:20} Importance: {}'.format(*pair))
