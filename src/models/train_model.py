import os

import xgboost as xgb
from pandas import DataFrame
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler

import src
from src.features.build_features import BuildFeatures

paths = src.config.DEFAULT.PATHS

TEST_SIZE = 0.2
RANDOM_STATE = 7


class TrainModel:
    def __init__(self, df):
        if not isinstance(df, DataFrame):
            raise TypeError
        self.df = df
        self.features = None
        self.labels = None
        self.x = None
        self.y = None
        self.data_dmatrix = None
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.scaler = None
        self.model = None
        self.y_pred = None

    def get_features_labels(self):
        """
        get data from self.x and self.y
        :return:
        """
        self.features = self.x.values[:, 1:]
        self.labels = self.y.values

    def initialize_scaler(self, scaler_type: str):
        """

        :param scaler_type:
        :return:
        """
        if not isinstance(scaler_type, str):
            raise TypeError
        if scaler_type == "MinMaxScaler":
            self.scaler = MinMaxScaler((-1, 1))
        elif scaler_type == "RobustScaler":
            self.scaler = RobustScaler()
        elif scaler_type == "StandardScaler":
            self.scaler = StandardScaler()

    def split_df_into_x_and_y(self, column_name: str):
        """
        split df into x and y set given the column_name
        :param column_name:
        :return:
        """
        if not isinstance(column_name, str):
            raise TypeError
        self.x = self.df.loc[:, self.df.columns != column_name]
        self.y = self.df.loc[:, column_name]

    def split_training_testing_sets(self):
        # self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
        #     self.scaler.fit_transform(self.features),
        #     self.labels, test_size=TEST_SIZE,
        #     random_state=RANDOM_STATE)
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(self.x,
                                                                                self.y, test_size=TEST_SIZE,
                                                                                random_state=RANDOM_STATE)

    def initiate_classifier(self, classifier: str):
        if not isinstance(classifier, str):
            raise TypeError
        if classifier == "XGBClassifier":
            self.model = xgb.XGBClassifier()
        elif classifier == "LinearDiscriminantAnalysis":
            self.model = LinearDiscriminantAnalysis()

    def fit_gradient_boosting_model(self):
        self.model.fit(self.x_train, self.y_train)

    def setup_classification_workflow(self, column_name: str):
        self.split_df_into_x_and_y(column_name=column_name)
        self.get_features_labels()
        # self.initialize_scaler("MinMaxScaler")
        self.split_training_testing_sets()
        self.initiate_classifier("LinearDiscriminantAnalysis")
        self.fit_gradient_boosting_model()


if __name__ == "__main__":
    combined_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "Combined_News_DJIA.csv")
    upload_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "upload_DJIA_table.csv")

    # Combine df of combined_news_file and upload_news_file
    build_features = BuildFeatures(filepath=combined_news_file)
    df2 = build_features.load_df_from(upload_news_file)
    build_features.df = build_features.df.merge(df2, how="inner", on="Date")
    built_df = build_features.workflow()

    keep_columns = ["open", "high", "low", "close", "volume", "subjectivity", "polarity", "compound", "negative",
                    "positive", "neutral", "label"]
    built_df = built_df[keep_columns]

    train_model = TrainModel(df=built_df)
    train_model.setup_classification_workflow(column_name="label")
