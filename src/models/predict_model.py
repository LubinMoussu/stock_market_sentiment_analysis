from src.models.features_importance import FeaturesImportance
import os

import numpy as np
from sklearn.metrics import accuracy_score, mean_squared_error, mean_absolute_error
from sklearn.metrics import classification_report
from sklearn.metrics import mean_absolute_percentage_error

import src
from src.features.build_features import BuildFeatures
from src.models.features_importance import FeaturesImportance
from src.models.train_model import TrainModel

paths = src.config.DEFAULT.PATHS


class PredictModel:
    def __init__(self, df, model, y_pred, x_test, y_test, x_train, y_train, x, feature_importance=FeaturesImportance()):
        self.df = df
        self.model = model
        self.y_pred = y_pred
        self.x_test = x_test
        self.y_test = y_test
        self.x_train = x_train
        self.y_train = y_train
        self.x = x
        self.feature_importance = feature_importance

    def generate_y_pred(self):
        self.y_pred = self.model.predict(self.x_test)

    def get_mae(self):
        """
        get mean_absolute_error from prediction
        :return:
        """
        return mean_absolute_error(self.y_test, self.y_pred)

    def get_rmse(self):
        """
        get root mean square error from prediction
        :return:
        """
        return np.sqrt(mean_squared_error(self.y_test, self.y_pred))

    def get_mape(self):
        """
        get mean_absolute_percentage_error from prediction
        :return:
        """
        return mean_absolute_percentage_error(self.y_test, self.y_pred)

    def get_feature_importances(self):
        self.feature_importance.set_model(self.model)
        self.feature_importance.set_feature_list(self.x.columns)
        self.feature_importance.get_feature_importance()

    def stats(self):
        self.generate_y_pred()
        print(accuracy_score(self.y_test, self.y_pred))

        print(classification_report(self.y_test, self.y_pred))
        print(self.get_mae())
        print(self.get_rmse())
        print(self.get_mape())

    def stats_feature_importances(self):
        self.get_feature_importances()
        lr = self.model

        print("lr.coef_: {}".format(lr.coef_))
        print("lr.intercept_: {}".format(lr.intercept_))
        print("Training set score: {:.2f}".format(lr.score(self.x_train, self.y_train)))
        print("Test set score: {:.7f}".format(lr.score(self.x_test, self.y_test)))


if __name__ == "__main__":
    combined_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "Combined_News_DJIA.csv")
    upload_news_file = os.path.join(src.ROOT_DIR, paths.DATA_RAW, "upload_DJIA_table.csv")

    # Combine df of combined_news_file and upload_news_file
    build_features = BuildFeatures(filepath=combined_news_file)
    df2 = build_features.load_df_from(upload_news_file)
    build_features.df = build_features.df.merge(df2, how="inner", on="Date")
    built_df = build_features.workflow()

    keep_columns = ["open", "high", "low", "close", "volume", "subjectivity", "polarity", "compound", "negative",
                    "positive", "neutral", "label"]
    built_df = built_df[keep_columns]

    train_model = TrainModel(df=built_df)
    train_model.setup_classification_workflow(column_name="label")

    predict_model = PredictModel(df=train_model.df, model=train_model.model, y_pred=train_model.y_pred,
                                 x_test=train_model.x_test, y_test=train_model.y_test, x_train=train_model.x_train,
                                 y_train=train_model.y_train, x=train_model.x)

    predict_model.stats()
    predict_model.stats_feature_importances()
